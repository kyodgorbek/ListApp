package com.example.listapp;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ListView listView = (ListView) findViewById(R.id.lvMainList);
        listView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
                getResources().getStringArray(R.array.list_objects_solar_system)));
        listView.setOnClickListener(new AdapterView<?>  parent, View view int postion, long id){
            Toast.makeText(view.getContext(), (String) parent.getAdapter().getItem(position),
                    Toast.LENGTH_LONG).show();
        }
    });
}

    @Override
    public boolean onCreateOptionMenu(Menu menu){
        //Inflate the menu; this adds items to the action bar if it is present
        getMenuInflater().inflate(R.menu.menu_main_list, menu);
        return true;
    }
